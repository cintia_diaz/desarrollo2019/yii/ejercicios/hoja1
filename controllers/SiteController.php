<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Entradas;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /* SELECT * FROM entradas; */
    
    public function actionListar()
    {
        $s= Entradas::find()->asArray()->all();
        
        return $this->render("listar",
                [
                    "datos"=>$s,
                ]);
    }
    
    /* otra forma de crear la consulta */
    
    public function actionListar1(){
        $s= Entradas::find()->all();
        
        return $this->render("listar1",
                [
                    "datos"=>$s,
                ]);
    }
    
    /* diversas formas de realizar consultas en las bases de datos */
    
    public function actionListar2(){
        $salida = Entradas::find()->select(['texto'])->asArray()->all();
        return $this->render('listar2',["datos"=>$salida]);
    }
    
    public function actionListar3(){
        $salida = Entradas::find()->select(['texto'])->all();
        return $this->render('listar3',["datos"=>$salida]);
    }
    
    public function actionListar4(){
        $salida = new Entradas();
        return $this->render('listar4',[
            "datos"=>$salida->find()->all()
                ]);
    }
    
    public function actionListar5(){
        $salida = new Entradas();
        return $this->render('listar5',[
            "datos"=>$salida->findOne(1),
                ]);
    }
    
    public function actionListar6(){
        return $this->render('listar6',[
            "datos"=>Yii::$app->db->createCommand("SELECT * FROM entradas")->queryAll(),
            ]);
    }
    
    /* otra accion: Mostrar */
    
    public function actionMostrar() {
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query' => Entradas::find(), 
        ]);
        
        return $this->render('mostrar', [
            'dataProvider' => $dataProvider,
        ]);       
    }
    
    /* accion para mostrar un solo registro a través del componente DetailView: Mostraruno */
    
    public function actionMostraruno(){
        return $this->render('mostraruno',[
            'model' => Entradas::findOne(1),
        ]);
    }
}

